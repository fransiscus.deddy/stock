CREATE TABLE public.brand (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    name character varying(250) NOT NULL
);


ALTER TABLE public.brand OWNER TO deddy;

--
-- Name: brand_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.brand_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.brand_seq OWNER TO deddy;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.customer (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    address1 character varying(250) NOT NULL,
    address2 character varying(250) NOT NULL,
    city character varying(100) NOT NULL,
    name character varying(250) NOT NULL,
    status integer
);


ALTER TABLE public.customer OWNER TO deddy;


--
-- Name: history_stock_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.history_stock_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.history_stock_seq OWNER TO deddy;

--
-- Name: lookup; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.lookup (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    code character varying(5),
    name character varying(250) NOT NULL,
    lookup_group character varying(255)
);


ALTER TABLE public.lookup OWNER TO deddy;

--
-- Name: lookup_group; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.lookup_group (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    name character varying(250) NOT NULL
);


ALTER TABLE public.lookup_group OWNER TO deddy;

--
-- Name: lookup_group_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.lookup_group_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lookup_group_seq OWNER TO deddy;

--
-- Name: lookup_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.lookup_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lookup_seq OWNER TO deddy;

--
-- Name: outlet; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.outlet (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    name character varying(250) NOT NULL,
    stock bigint,
    stock_value numeric(19,2),
    outlet_product character varying(255),
    outlet_store character varying(255)
);


ALTER TABLE public.outlet OWNER TO deddy;

--
-- Name: outlet_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.outlet_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.outlet_seq OWNER TO deddy;

--
-- Name: payment_detail_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.payment_detail_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_detail_seq OWNER TO deddy;

--
-- Name: payment_dt; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.payment_dt (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    total numeric(19,2),
    payment_dt_payment_hd character varying(255),
    payment_receiving character varying(255)
);


ALTER TABLE public.payment_dt OWNER TO deddy;

--
-- Name: payment_hd; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.payment_hd (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    description character varying(250),
    payment_due_date date,
    payment_number character varying(10),
    payment_reff_number character varying(255),
    payment_type character varying(255),
    status integer,
    total numeric(19,2),
    transaction_date date,
    payment_supplier character varying(255)
);


ALTER TABLE public.payment_hd OWNER TO deddy;

--
-- Name: payment_hd_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.payment_hd_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_hd_seq OWNER TO deddy;

--
-- Name: person_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.person_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_seq OWNER TO deddy;

--
-- Name: po_detail_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.po_detail_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.po_detail_seq OWNER TO deddy;

--
-- Name: po_dt; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.po_dt (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    qty bigint,
    unit character varying(255),
    po_detail_product character varying(255),
    po_dt_po_hd character varying(255)
);


ALTER TABLE public.po_dt OWNER TO deddy;

--
-- Name: po_hd; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.po_hd (
    id character varying(255) NOT NULL,
    description character varying(250),
    expired_po_date date,
    po_number character varying(10),
    status integer,
    transaction_date date,
    po_supplier character varying(255)
);


ALTER TABLE public.po_hd OWNER TO deddy;

--
-- Name: po_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.po_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.po_seq OWNER TO deddy;

--
-- Name: product; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.product (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    code character varying(20),
    content bigint,
    large_unit character varying(255),
    name character varying(250) NOT NULL,
    sales_price numeric(19,2),
    status integer,
    stocked boolean,
    unit character varying(255),
    product_group character varying(255)
);


ALTER TABLE public.product OWNER TO deddy;

--
-- Name: product_group; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.product_group (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    name character varying(250) NOT NULL,
    status integer
);


ALTER TABLE public.product_group OWNER TO deddy;

--
-- Name: product_group_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.product_group_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_group_seq OWNER TO deddy;

--
-- Name: product_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.product_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_seq OWNER TO deddy;

--
-- Name: receiving_detail_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.receiving_detail_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.receiving_detail_seq OWNER TO deddy;

--
-- Name: receiving_dt; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.receiving_dt (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    disc1 numeric(19,2),
    disc2 numeric(19,2),
    free_item boolean,
    price numeric(19,2),
    qty bigint,
    unit character varying(255),
    receiving_detail_product character varying(255),
    receiving_dt_receiving_hd character varying(255)
);


ALTER TABLE public.receiving_dt OWNER TO deddy;

--
-- Name: receiving_hd; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.receiving_hd (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    description character varying(250) NOT NULL,
    disc numeric(19,2),
    frompo boolean,
    grand_total numeric(19,2),
    po_number character varying(255),
    ppn numeric(19,2),
    receiving_number character varying(10),
    status integer,
    sub_total numeric(19,2),
    transaction_date date,
    receive_supplier character varying(255)
);


ALTER TABLE public.receiving_hd OWNER TO deddy;

--
-- Name: receiving_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.receiving_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.receiving_seq OWNER TO deddy;

--
-- Name: return_detail_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.return_detail_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.return_detail_seq OWNER TO deddy;

--
-- Name: return_dt; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.return_dt (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    price numeric(19,2),
    qty bigint,
    unit character varying(255),
    return_detail_product character varying(255),
    return_dt_return_hd character varying(255)
);


ALTER TABLE public.return_dt OWNER TO deddy;

--
-- Name: return_hd; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.return_hd (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    description character varying(250),
    po_date date,
    ref_number character varying(255),
    ret_purchase boolean,
    retur_number character varying(10),
    status integer,
    return_customer character varying(255),
    return_supplier character varying(255)
);


ALTER TABLE public.return_hd OWNER TO deddy;

--
-- Name: return_hd_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.return_hd_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.return_hd_seq OWNER TO deddy;

--
-- Name: sales_detail_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.sales_detail_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sales_detail_seq OWNER TO deddy;

--
-- Name: sales_dt; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.sales_dt (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    disc1 numeric(19,2),
    disc2 numeric(19,2),
    free_item boolean,
    price numeric(19,2),
    qty bigint,
    unit character varying(255),
    sales_dt_product character varying(255),
    sales_dt_sales_hd character varying(255)
);


ALTER TABLE public.sales_dt OWNER TO deddy;

--
-- Name: sales_hd; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.sales_hd (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    description character varying(250) NOT NULL,
    disc numeric(19,2),
    grand_total numeric(19,2),
    invoice_number character varying(10),
    ppn numeric(19,2),
    status integer,
    sub_total numeric(19,2),
    transaction_date date,
    sales_customer character varying(255),
    sales_salesman character varying(255)
);


ALTER TABLE public.sales_hd OWNER TO deddy;

--
-- Name: sales_hd_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.sales_hd_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sales_hd_seq OWNER TO deddy;

--
-- Name: salesman; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.salesman (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    address1 character varying(250) NOT NULL,
    address2 character varying(250) NOT NULL,
    city character varying(100) NOT NULL,
    name character varying(250) NOT NULL,
    status integer,
    date_of_birth date,
    place_of_birth character varying(255),
    salesman_code character varying(7)
);


ALTER TABLE public.salesman OWNER TO deddy;

--
-- Name: stock_history; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.stock_history (
    id character varying(255) NOT NULL,
    debet bigint,
    description character varying(250) NOT NULL,
    kredit bigint,
    saldo bigint,
    history_product character varying(255)
);


ALTER TABLE public.stock_history OWNER TO deddy;

--
-- Name: store; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.store (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    address1 character varying(250),
    address2 character varying(250),
    city character varying(100) NOT NULL,
    name character varying(250) NOT NULL,
    status integer
);


ALTER TABLE public.store OWNER TO deddy;

--
-- Name: store_seq; Type: SEQUENCE; Schema: public; Owner: deddy
--

CREATE SEQUENCE public.store_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.store_seq OWNER TO deddy;

--
-- Name: supplier; Type: TABLE; Schema: public; Owner: deddy
--

CREATE TABLE public.supplier (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    address1 character varying(250) NOT NULL,
    address2 character varying(250) NOT NULL,
    city character varying(100) NOT NULL,
    name character varying(250) NOT NULL,
    status integer,
    account_name character varying(255),
    account_number character varying(255),
    bank character varying(255),
    branch character varying(255),
    top integer
);


ALTER TABLE public.supplier OWNER TO deddy;

--
-- Data for Name: brand; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.brand (id, created_at, created_by, updated_at, updated_by, name) FROM stdin;
\.


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.customer (id, created_at, created_by, updated_at, updated_by, address1, address2, city, name, status) FROM stdin;
\.


--
-- Data for Name: lookup; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.lookup (id, created_at, created_by, updated_at, updated_by, code, name, lookup_group) FROM stdin;
\.


--
-- Data for Name: lookup_group; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.lookup_group (id, created_at, created_by, updated_at, updated_by, name) FROM stdin;
\.


--
-- Data for Name: outlet; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.outlet (id, created_at, created_by, updated_at, updated_by, name, stock, stock_value, outlet_product, outlet_store) FROM stdin;
\.


--
-- Data for Name: payment_dt; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.payment_dt (id, created_at, created_by, updated_at, updated_by, total, payment_dt_payment_hd, payment_receiving) FROM stdin;
\.


--
-- Data for Name: payment_hd; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.payment_hd (id, created_at, created_by, updated_at, updated_by, description, payment_due_date, payment_number, payment_reff_number, payment_type, status, total, transaction_date, payment_supplier) FROM stdin;
\.


--
-- Data for Name: po_dt; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.po_dt (id, created_at, created_by, updated_at, updated_by, qty, unit, po_detail_product, po_dt_po_hd) FROM stdin;
\.


--
-- Data for Name: po_hd; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.po_hd (id, description, expired_po_date, po_number, status, transaction_date, po_supplier) FROM stdin;
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.product (id, created_at, created_by, updated_at, updated_by, code, content, large_unit, name, sales_price, status, stocked, unit, product_group) FROM stdin;
\.


--
-- Data for Name: product_group; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.product_group (id, created_at, created_by, updated_at, updated_by, name, status) FROM stdin;
\.


--
-- Data for Name: receiving_dt; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.receiving_dt (id, created_at, created_by, updated_at, updated_by, disc1, disc2, free_item, price, qty, unit, receiving_detail_product, receiving_dt_receiving_hd) FROM stdin;
\.


--
-- Data for Name: receiving_hd; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.receiving_hd (id, created_at, created_by, updated_at, updated_by, description, disc, frompo, grand_total, po_number, ppn, receiving_number, status, sub_total, transaction_date, receive_supplier) FROM stdin;
\.


--
-- Data for Name: return_dt; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.return_dt (id, created_at, created_by, updated_at, updated_by, price, qty, unit, return_detail_product, return_dt_return_hd) FROM stdin;
\.


--
-- Data for Name: return_hd; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.return_hd (id, created_at, created_by, updated_at, updated_by, description, po_date, ref_number, ret_purchase, retur_number, status, return_customer, return_supplier) FROM stdin;
\.


--
-- Data for Name: sales_dt; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.sales_dt (id, created_at, created_by, updated_at, updated_by, disc1, disc2, free_item, price, qty, unit, sales_dt_product, sales_dt_sales_hd) FROM stdin;
\.


--
-- Data for Name: sales_hd; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.sales_hd (id, created_at, created_by, updated_at, updated_by, description, disc, grand_total, invoice_number, ppn, status, sub_total, transaction_date, sales_customer, sales_salesman) FROM stdin;
\.


--
-- Data for Name: salesman; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.salesman (id, created_at, created_by, updated_at, updated_by, address1, address2, city, name, status, date_of_birth, place_of_birth, salesman_code) FROM stdin;
\.


--
-- Data for Name: stock_history; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.stock_history (id, debet, description, kredit, saldo, history_product) FROM stdin;
\.


--
-- Data for Name: store; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.store (id, created_at, created_by, updated_at, updated_by, address1, address2, city, name, status) FROM stdin;
\.


--
-- Data for Name: supplier; Type: TABLE DATA; Schema: public; Owner: deddy
--

COPY public.supplier (id, created_at, created_by, updated_at, updated_by, address1, address2, city, name, status, account_name, account_number, bank, branch, top) FROM stdin;
\.


--
-- Name: brand_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.brand_seq', 1, false);


--
-- Name: history_stock_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.history_stock_seq', 1, false);


--
-- Name: lookup_group_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.lookup_group_seq', 1, false);


--
-- Name: lookup_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.lookup_seq', 1, false);


--
-- Name: outlet_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.outlet_seq', 1, false);


--
-- Name: payment_detail_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.payment_detail_seq', 1, false);


--
-- Name: payment_hd_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.payment_hd_seq', 1, false);


--
-- Name: person_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.person_seq', 1, false);


--
-- Name: po_detail_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.po_detail_seq', 1, false);


--
-- Name: po_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.po_seq', 1, false);


--
-- Name: product_group_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.product_group_seq', 1, false);


--
-- Name: product_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.product_seq', 1, false);


--
-- Name: receiving_detail_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.receiving_detail_seq', 1, false);


--
-- Name: receiving_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.receiving_seq', 1, false);


--
-- Name: return_detail_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.return_detail_seq', 1, false);


--
-- Name: return_hd_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.return_hd_seq', 1, false);


--
-- Name: sales_detail_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.sales_detail_seq', 1, false);


--
-- Name: sales_hd_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.sales_hd_seq', 1, false);


--
-- Name: store_seq; Type: SEQUENCE SET; Schema: public; Owner: deddy
--

SELECT pg_catalog.setval('public.store_seq', 1, false);


--
-- Name: brand brand_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.brand
    ADD CONSTRAINT brand_pkey PRIMARY KEY (id);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);

--
-- Name: lookup_group lookup_group_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.lookup_group
    ADD CONSTRAINT lookup_group_pkey PRIMARY KEY (id);


--
-- Name: lookup lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.lookup
    ADD CONSTRAINT lookup_pkey PRIMARY KEY (id);


--
-- Name: outlet outlet_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.outlet
    ADD CONSTRAINT outlet_pkey PRIMARY KEY (id);


--
-- Name: payment_dt payment_dt_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.payment_dt
    ADD CONSTRAINT payment_dt_pkey PRIMARY KEY (id);


--
-- Name: payment_hd payment_hd_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.payment_hd
    ADD CONSTRAINT payment_hd_pkey PRIMARY KEY (id);


--
-- Name: po_dt po_dt_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.po_dt
    ADD CONSTRAINT po_dt_pkey PRIMARY KEY (id);


--
-- Name: po_hd po_hd_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.po_hd
    ADD CONSTRAINT po_hd_pkey PRIMARY KEY (id);


--
-- Name: product_group product_group_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.product_group
    ADD CONSTRAINT product_group_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: receiving_dt receiving_dt_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.receiving_dt
    ADD CONSTRAINT receiving_dt_pkey PRIMARY KEY (id);


--
-- Name: receiving_hd receiving_hd_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.receiving_hd
    ADD CONSTRAINT receiving_hd_pkey PRIMARY KEY (id);


--
-- Name: return_dt return_dt_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.return_dt
    ADD CONSTRAINT return_dt_pkey PRIMARY KEY (id);


--
-- Name: return_hd return_hd_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.return_hd
    ADD CONSTRAINT return_hd_pkey PRIMARY KEY (id);


--
-- Name: sales_dt sales_dt_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.sales_dt
    ADD CONSTRAINT sales_dt_pkey PRIMARY KEY (id);


--
-- Name: sales_hd sales_hd_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.sales_hd
    ADD CONSTRAINT sales_hd_pkey PRIMARY KEY (id);


--
-- Name: salesman salesman_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.salesman
    ADD CONSTRAINT salesman_pkey PRIMARY KEY (id);


--
-- Name: stock_history stock_history_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.stock_history
    ADD CONSTRAINT stock_history_pkey PRIMARY KEY (id);


--
-- Name: store store_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.store
    ADD CONSTRAINT store_pkey PRIMARY KEY (id);


--
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (id);


--
-- Name: sales_hd fk2fu7qm2a64hcec4p1h432sthh; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.sales_hd
    ADD CONSTRAINT fk2fu7qm2a64hcec4p1h432sthh FOREIGN KEY (sales_customer) REFERENCES public.customer(id);


--
-- Name: receiving_dt fk2q1ny1xyc27gsvtpqtyeauqfb; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.receiving_dt
    ADD CONSTRAINT fk2q1ny1xyc27gsvtpqtyeauqfb FOREIGN KEY (receiving_dt_receiving_hd) REFERENCES public.receiving_hd(id);


--
-- Name: product fk58i50ngmphh08djmvs3tiw4t4; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT fk58i50ngmphh08djmvs3tiw4t4 FOREIGN KEY (product_group) REFERENCES public.product_group(id);


--
-- Name: sales_dt fk7337noie4ilblwp4trh37fyif; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.sales_dt
    ADD CONSTRAINT fk7337noie4ilblwp4trh37fyif FOREIGN KEY (sales_dt_product) REFERENCES public.product(id);


--
-- Name: receiving_hd fk7d2a9kfy4ivgvcyrllicwpe26; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.receiving_hd
    ADD CONSTRAINT fk7d2a9kfy4ivgvcyrllicwpe26 FOREIGN KEY (receive_supplier) REFERENCES public.supplier(id);


--
-- Name: payment_dt fk8gtvmvpj7uxoq4cijxt1md2xi; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.payment_dt
    ADD CONSTRAINT fk8gtvmvpj7uxoq4cijxt1md2xi FOREIGN KEY (payment_receiving) REFERENCES public.receiving_hd(id);


--
-- Name: lookup fk8w36x3nxap7q73jeayw4qleyw; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.lookup
    ADD CONSTRAINT fk8w36x3nxap7q73jeayw4qleyw FOREIGN KEY (lookup_group) REFERENCES public.lookup_group(id);


--
-- Name: sales_dt fk9629nw8mkkfovt0deemms20kv; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.sales_dt
    ADD CONSTRAINT fk9629nw8mkkfovt0deemms20kv FOREIGN KEY (sales_dt_sales_hd) REFERENCES public.sales_hd(id);


--
-- Name: receiving_dt fk9gbfq0vcomnrj8boc2kaa7egn; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.receiving_dt
    ADD CONSTRAINT fk9gbfq0vcomnrj8boc2kaa7egn FOREIGN KEY (receiving_detail_product) REFERENCES public.product(id);


--
-- Name: payment_dt fkb390jcdfaac9opxw4aihwql3g; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.payment_dt
    ADD CONSTRAINT fkb390jcdfaac9opxw4aihwql3g FOREIGN KEY (payment_dt_payment_hd) REFERENCES public.payment_hd(id);


--
-- Name: stock_history fkeyw3su1uackxf19wb9gaeaj72; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.stock_history
    ADD CONSTRAINT fkeyw3su1uackxf19wb9gaeaj72 FOREIGN KEY (history_product) REFERENCES public.product(id);


--
-- Name: po_dt fkgbf8k8mxdar318xjyf11ysqr9; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.po_dt
    ADD CONSTRAINT fkgbf8k8mxdar318xjyf11ysqr9 FOREIGN KEY (po_detail_product) REFERENCES public.product(id);


--
-- Name: return_hd fkgckqnqcdfpwiuc6y47ku51aej; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.return_hd
    ADD CONSTRAINT fkgckqnqcdfpwiuc6y47ku51aej FOREIGN KEY (return_supplier) REFERENCES public.supplier(id);


--
-- Name: return_dt fkhb84cm8oidahbmeeyd0fyq31f; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.return_dt
    ADD CONSTRAINT fkhb84cm8oidahbmeeyd0fyq31f FOREIGN KEY (return_dt_return_hd) REFERENCES public.return_hd(id);


--
-- Name: po_dt fkhq04xxnk9qcgrq2i407nomdiw; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.po_dt
    ADD CONSTRAINT fkhq04xxnk9qcgrq2i407nomdiw FOREIGN KEY (po_dt_po_hd) REFERENCES public.po_hd(id);


--
-- Name: return_dt fkhvee01237hschb1sxnla35opb; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.return_dt
    ADD CONSTRAINT fkhvee01237hschb1sxnla35opb FOREIGN KEY (return_detail_product) REFERENCES public.product(id);


--
-- Name: return_hd fkhx4iquu8c8fd7kcclnkongfw8; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.return_hd
    ADD CONSTRAINT fkhx4iquu8c8fd7kcclnkongfw8 FOREIGN KEY (return_customer) REFERENCES public.customer(id);


--
-- Name: outlet fkl9c0idqb5d2bag02kk29er75p; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.outlet
    ADD CONSTRAINT fkl9c0idqb5d2bag02kk29er75p FOREIGN KEY (outlet_store) REFERENCES public.store(id);


--
-- Name: outlet fkncopl6343dupti87tryh31qu5; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.outlet
    ADD CONSTRAINT fkncopl6343dupti87tryh31qu5 FOREIGN KEY (outlet_product) REFERENCES public.product(id);


--
-- Name: sales_hd fkqe63hyh08ujaetp1lp3dotxkx; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.sales_hd
    ADD CONSTRAINT fkqe63hyh08ujaetp1lp3dotxkx FOREIGN KEY (sales_salesman) REFERENCES public.salesman(id);


--
-- Name: payment_hd fkqxmd4nbaq2tkax50obgsjrb0x; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.payment_hd
    ADD CONSTRAINT fkqxmd4nbaq2tkax50obgsjrb0x FOREIGN KEY (payment_supplier) REFERENCES public.supplier(id);


--
-- Name: po_hd fktffynpj9ibe0w6ckpug7dmihn; Type: FK CONSTRAINT; Schema: public; Owner: deddy
--

ALTER TABLE ONLY public.po_hd
    ADD CONSTRAINT fktffynpj9ibe0w6ckpug7dmihn FOREIGN KEY (po_supplier) REFERENCES public.supplier(id);


--
-- PostgreSQL database dump complete
--

