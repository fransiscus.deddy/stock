package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="brand", schema = "public")
public class Brand extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_brand_seq", sequenceName = "brand_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_brand_seq")
    private String id;

    @Column(length = 250, nullable = false)
    private String name;

}
