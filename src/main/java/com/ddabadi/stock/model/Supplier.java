package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Person;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Table(name="supplier", schema = "public")
public class Supplier extends Person implements Serializable {

    // term of payment
    // hari
    @Column
    private Integer top;

    @Column
    private String bank;

    @Column
    private String branch;

    @Column
    private String accountNumber;

    @Column
    private String accountName;

}
