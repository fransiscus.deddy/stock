package com.ddabadi.stock.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="stock_history", schema = "public")
public class StockHistory implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_history_stock_seq", sequenceName = "history_stock_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_history_stock_seq")
    private String id;

    @ManyToOne
    @JoinColumn(name = "history_product")
    private Product product;

    @Column(length = 250, nullable = false)
    private String description;

    @Column
    private Long debet;

    @Column
    private Long kredit;

    @Column
    private Long saldo;
}
