package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="receiving_dt", schema = "public")
public class ReceivingDt extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_receiving_detail_seq", sequenceName = "receiving_detail_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_receiving_detail_seq")
    private String id;

    @ManyToOne
    @JoinColumn(name = "receiving_dt_receiving_hd")
    private ReceivingHd receivingHd;

    @ManyToOne
    @JoinColumn(name = "receiving_detail_product")
    private Product product;

    @Column
    private Long qty;

    @Formula("select l.name from lookup l where l.code = unit ")
    private String unitDesc;

    @Column
    private String unit;

    @Column
    private BigDecimal price;

    @Column
    private BigDecimal disc1;

    @Column
    private BigDecimal disc2;

    @Column
    private Boolean freeItem;

}
