package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="po_dt", schema = "public")
public class PurchaseOrderDt extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_po_detail_seq", sequenceName = "po_detail_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_po_detail_seq")
    private String id;

    @ManyToOne
    @JoinColumn(name = "po_dt_po_hd")
    private PurchaseOrderHd purchaseOrderHd;

    @ManyToOne
    @JoinColumn(name = "po_detail_product")
    private Product product;

    @Column
    private Long qty;

    @Formula("select l.name from lookup l where l.code = unit ")
    private String unitDesc;

    @Column
    private String unit;

}
