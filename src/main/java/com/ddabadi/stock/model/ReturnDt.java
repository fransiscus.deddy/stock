package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="return_dt", schema = "public")
public class ReturnDt extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_return_dt_seq", sequenceName = "return_detail_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_return_dt_seq")
    private String id;

    @ManyToOne
    @JoinColumn(name = "return_dt_return_hd")
    private ReturnHd returnHd;

    @ManyToOne
    @JoinColumn(name = "return_detail_product")
    private Product product;

    @Column
    private Long qty;

    @Transient
    @Formula("select l.name from lookup l where l.code = unit ")
    private String unitDesc;

    @Column
    private String unit;

    @Column
    private BigDecimal price;

}
