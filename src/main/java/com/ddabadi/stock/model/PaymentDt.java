package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="payment_dt", schema = "public")
public class PaymentDt extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_payment_detail_seq", sequenceName = "payment_detail_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_payment_detail_seq")
    private String id;

    @ManyToOne
    @JoinColumn(name = "payment_dt_payment_hd")
    private PaymentHd paymentHd;

    @ManyToOne
    @JoinColumn(name = "payment_receiving")
    private ReceivingHd receivingHd;

    @Column
    private BigDecimal total;

}
