package com.ddabadi.stock.model.base;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
public abstract  class Base implements Serializable {

    @Column
    private String updatedBy;

    @Column
    private String createdBy;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @PrePersist
    public void prePersist(){
        this.updatedAt = new Date();
        this.createdAt = new Date();
    };

    @PreUpdate
    public void preUpdate(){
        this.updatedAt = new Date();
    }

}
