package com.ddabadi.stock.model.base;

import com.ddabadi.stock.model.enumerate.EntityStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Person extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_person_seq", sequenceName = "person_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_person_seq")
    private String id;

    @Column(length = 250, nullable = false)
    private String name;

    @Column(length = 250, nullable = false)
    private String address1;

    @Column(length = 250, nullable = false)
    private String address2;

    @Column(length = 100, nullable = false)
    private String city;

    @Column()
    private EntityStatus status;

}
