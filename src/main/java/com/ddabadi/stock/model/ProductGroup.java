package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import com.ddabadi.stock.model.enumerate.EntityStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="product_group", schema = "public")
public class ProductGroup extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_product_group_seq", sequenceName = "product_group_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_product_group_seq")
    private String id;

    @Column(length = 250, nullable = false)
    private String name;

    @Column
    private EntityStatus status;

    @PrePersist
    public void prePersist(){
        this.status = EntityStatus.ACTIVE;
    };

}
