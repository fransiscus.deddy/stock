package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="lookup", schema = "public")
public class Lookup extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_lookup_seq", sequenceName = "lookup_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_lookup_seq")
    private String id;

    @Column(length = 250, nullable = false)
    private String name;

    @Column(length = 5)
    private String code;

    @ManyToOne
    @JoinColumn(name = "lookup_group")
    private LookupGroup lookupGroup;

}
