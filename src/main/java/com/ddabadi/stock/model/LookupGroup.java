package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import com.ddabadi.stock.model.enumerate.EntityStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="lookup_group", schema = "public")
public class LookupGroup extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_lookup_group_seq", sequenceName = "lookup_group_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_lookup_group_seq")
    private String id;

    @Column(length = 250, nullable = false)
    private String name;


}
