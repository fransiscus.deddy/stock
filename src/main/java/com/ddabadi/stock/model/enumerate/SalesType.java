package com.ddabadi.stock.model.enumerate;

public enum SalesType {
    CASH("CASH"),
    CREDIT("CREDIT");

    private String statusCode;

    SalesType(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }
}
