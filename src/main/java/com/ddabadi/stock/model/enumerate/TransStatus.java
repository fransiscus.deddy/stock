package com.ddabadi.stock.model.enumerate;

public enum TransStatus {
    NOT_APPROVED("NOT_APPROVED"),
    APPROVED ("APPROVED"),
    RECEIVED ("RECEIVED"),
    PAID ("PAID"),
    CANCEL  ("CANCEL");

    private String statusCode;

    TransStatus(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

}
