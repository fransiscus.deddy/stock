package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import com.ddabadi.stock.model.enumerate.EntityStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="store", schema = "public")
public class Store extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_store_seq", sequenceName = "store_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_store_seq")
    private String id;

    @Column(length = 250, nullable = false)
    private String name;

    @Column(length = 250)
    private String address1;

    @Column(length = 250)
    private String address2;

    @Column(length = 100, nullable = false)
    private String city;

    @Column
    private EntityStatus status;
}
