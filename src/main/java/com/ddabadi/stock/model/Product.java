package com.ddabadi.stock.model;

import com.ddabadi.stock.model.base.Base;
import com.ddabadi.stock.model.enumerate.EntityStatus;
import lombok.*;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name="product", schema = "public")
public class Product extends Base implements Serializable {

    @Id
    @SequenceGenerator(name = "pk_product_seq", sequenceName = "product_seq", allocationSize=1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="pk_product_seq")
    private String id;

    @ManyToOne
    @JoinColumn(name = "product_group")
    private ProductGroup productGroup;

    @Column(length = 250, nullable = false)
    private String name;

    @Column(length = 20)
    private String code;

    @Formula("(select l.name from lookup l where l.code = unit) ")
    private String unitDesc;

    @Column
    private String unit;

    @Column
    private Long content;

    @Column
    private String largeUnit;

    @Column
    private EntityStatus status;

    @Column
    private Boolean stocked;

    @Column
    private BigDecimal salesPrice;


    @Formula("(select l.name from lookup l where l.code = large_unit ) ")
    private String largeUnitDesc;


    @PrePersist
    public void prePersist(){
       this.status = EntityStatus.ACTIVE;
    };

}
