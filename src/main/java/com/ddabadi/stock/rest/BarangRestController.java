package com.ddabadi.stock.rest;

import com.ddabadi.stock.model.Product;
import com.ddabadi.stock.repository.ProductRepository;
import com.ddabadi.stock.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/product")
public class BarangRestController {

    @Autowired private ProductService productService;

    @GetMapping("page/{page}/size/{size}")
    public Page<Product> findProduct(@PathVariable int page,
                                     @PathVariable int size){
        return productService.getAllProduct(page,size);
    }


}
