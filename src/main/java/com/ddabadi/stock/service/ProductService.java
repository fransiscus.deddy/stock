package com.ddabadi.stock.service;


import com.ddabadi.stock.model.Product;
import com.ddabadi.stock.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public Page<Product> getAllProduct(int page, int size) {

        PageRequest pageRequest = PageRequest.of(page -1, size);
        return repository.findAll(pageRequest);
    }


}
