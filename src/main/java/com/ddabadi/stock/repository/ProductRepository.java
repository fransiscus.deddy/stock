package com.ddabadi.stock.repository;

import com.ddabadi.stock.model.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductRepository extends PagingAndSortingRepository<Product, String> {
}
